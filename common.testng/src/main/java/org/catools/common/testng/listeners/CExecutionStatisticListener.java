package org.catools.common.testng.listeners;

import lombok.Getter;
import org.catools.common.collections.CHashMap;
import org.catools.common.testng.model.CExecutionStatus;
import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;
import org.testng.internal.IResultListener;

public class CExecutionStatisticListener implements ISuiteListener, IResultListener {
  private static final CHashMap<String, CExecutionStatus> methodSignatures = new CHashMap<>();
  @Getter
  private static int total;
  @Getter
  private static int passed;
  @Getter
  private static int failed;
  @Getter
  private static int skipped;
  @Getter
  private static int running;
  @Getter
  private static int waiting;

  private static void updateVariables() {
    total = methodSignatures.size();
    passed = methodSignatures.getAllKeys(CExecutionStatus::isPassed).size();
    failed = methodSignatures.getAllKeys(CExecutionStatus::isFailed).size();
    skipped = methodSignatures.getAllKeys(CExecutionStatus::isSkipped).size();
    running = methodSignatures.getAllKeys(CExecutionStatus::isRunning).size();
    waiting = total - passed - failed - skipped - running;
  }

  private static synchronized void updateTestResult(ITestNGMethod method, CExecutionStatus status) {
    methodSignatures.put(method.getTestClass().getName() + method.getMethodName(), status);
    updateVariables();
  }

  public static synchronized void removeTestMethod(ITestNGMethod method) {
    methodSignatures.remove(method.getTestClass().getName() + method.getMethodName());
    updateVariables();
  }

  @Override
  public void onStart(ISuite suite) {
    methodSignatures.clear();
    suite.getAllMethods().forEach(m -> updateTestResult(m, CExecutionStatus.CREATED));
  }

  @Override
  public void onTestStart(ITestResult result) {
    updateTestResult(result.getMethod(), CExecutionStatus.WIP);
  }

  @Override
  public void onTestSuccess(ITestResult result) {
    updateTestResult(result.getMethod(), CExecutionStatus.SUCCESS);
  }

  @Override
  public void onTestFailure(ITestResult result) {
    updateTestResult(result.getMethod(), CExecutionStatus.FAILURE);
  }

  @Override
  public void onTestSkipped(ITestResult result) {
    updateTestResult(result.getMethod(), CExecutionStatus.SKIP);
  }

  @Override
  public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
    updateTestResult(result.getMethod(), CExecutionStatus.SUCCESS);
  }
}
