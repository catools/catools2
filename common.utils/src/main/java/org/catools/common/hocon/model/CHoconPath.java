package org.catools.common.hocon.model;

public interface CHoconPath {
  String getPath();
}
