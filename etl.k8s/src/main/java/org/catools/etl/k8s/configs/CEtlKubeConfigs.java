package org.catools.etl.k8s.configs;

import lombok.experimental.UtilityClass;

@UtilityClass
public class CEtlKubeConfigs {
  /**
   * K8s Schema
   */
  public static final String K8S_SCHEMA = "kube";
}
