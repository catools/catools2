package org.catools.media.exception;

import org.catools.common.exception.CRuntimeException;

public class CUnSupportedTypeException extends CRuntimeException {
  public CUnSupportedTypeException(String message) {
    super(message);
  }
}
