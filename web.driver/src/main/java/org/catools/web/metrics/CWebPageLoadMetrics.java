package org.catools.web.metrics;

import lombok.Data;
import org.catools.common.collections.CList;

@Data
public class CWebPageLoadMetrics extends CList<CWebPageLoadMetric> {
}
