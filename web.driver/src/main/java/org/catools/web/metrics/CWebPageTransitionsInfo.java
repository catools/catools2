package org.catools.web.metrics;

import lombok.Data;
import org.catools.common.collections.CList;

@Data
public class CWebPageTransitionsInfo extends CList<CWebPageTransitionInfo> {
}
