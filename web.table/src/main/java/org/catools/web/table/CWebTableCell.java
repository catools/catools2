package org.catools.web.table;

public record CWebTableCell(int index, String header, String value, boolean visible) {
}
