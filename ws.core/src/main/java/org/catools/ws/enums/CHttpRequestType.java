package org.catools.ws.enums;

public enum CHttpRequestType {
  GET,
  HEAD,
  POST,
  PUT,
  DELETE,
  PATCH,
  OPTIONS
}
